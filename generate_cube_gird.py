import bpy
import math
import random

bpy.ops.object.select_all( action = 'SELECT' )
bpy.ops.object.delete()
bpy.ops.object.empty_add()
root = bpy.context.active_object

mats = [ bpy.data.materials.new(name="m0"), bpy.data.materials.new(name="m1"), bpy.data.materials.new(name="m2") ]

mats[0].diffuse_color = [1,0,0,1]
mats[1].diffuse_color = [0,1,0,1]
mats[2].diffuse_color = [0,0,1,1]

gridsize = 2

for z in range( -gridsize, gridsize + 1 ):
    for y in range( -gridsize, gridsize + 1 ):
        for x in range( -gridsize, gridsize + 1 ):
            # creation du cube
            bpy.ops.mesh.primitive_cube_add()
            # récuperation de l'objet créé
            cube = bpy.context.active_object
            # création d'un slot material
            bpy.ops.object.material_slot_add()
            # selection aléatoire de matériau
            mid = int( random.random() * len(mats)+1 ) - 1
            cube.material_slots[0].material = mats[ mid ]
            # renommage
            cube.name = 'c_' + str(x) + '_' + str( y ) + '_' + str( z )
            # position du cube
            cube.location.x = x * 2.5
            cube.location.y = y * 2.5
            cube.location.z = z * 2.5
            # rotation du cube
            cube.rotation_euler.x = random.random() * math.pi * 2
            cube.rotation_euler.y = random.random() * math.pi * 2
            cube.rotation_euler.z = random.random() * math.pi * 2
            # scale
            cube.scale.x = random.random() * 3
            cube.scale.y = random.random()
            cube.scale.z = random.random()
            # parentage
            cube.parent = root