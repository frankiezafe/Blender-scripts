import bpy
from mathutils import Color

mat_ground = "ATVB_mat_ground"
mat_road = "ATVB_mat_road"
mat_walls = "ATVB_mat_walls"
mat_roof = "ATVB_mat_roof"

def generate_mat( name, color ):
    
    if name in bpy.data.materials.keys():
        print( "material " + name + " exists" )
    else:
        mat = bpy.data.materials.new( name )
        mat.diffuse_color = color
        print( "material " + name + " created" )

def generate_mats():
    
    global mat_ground
    global mat_road
    global mat_walls
    global mat_roof
    
    generate_mat( mat_ground, ( 1,0,0 ) )
    generate_mat( mat_road, ( 0,1,0 ) )
    generate_mat( mat_walls, ( 1,0,1 ) )
    generate_mat( mat_roof, ( 0,1,1 ) )
    
generate_mats()