import os
import sys
import io
import bpy
import math
import mathutils
import random
import time
from contextlib import redirect_stdout

'''
== Across The Virtual Bridge ==

This scripts process export of vectiler.

=== I/O ===

It requires a specific structure:
* a folder named [name_city] to be present in the import folder
* in this folder, it awaits 2 files, first named "roads.obj" and second "buildings.obj"
* "roads.obj" must contain ground info (tiles of 2x2) - they are extracted from this file - all other objects in this file will be considered to be roads.
* "buildings.obj" must building objects

Generated models will be exported in ... export_folder

=== Naming convention ===

loading "roads.obj" is mandatory, even if no grounds or roads are generated: the global bounding box ('aabb') and the tiles info
are computed during this phase.
Tiles info ( see var 'tiles' ) is mandatory to compute the tile index stored in the name of the fbx.

* grounds: [name_prefix]_[name_city]_[tiles.index]_[name_ground]_[distance to tile center.x]x[distance to tile center.y].fbx
* roads: [name_prefix]_[name_city]_[tiles.index]_[name_road]_[distance to tile center.x]x[distance to tile center.y].fbx
* building: [name_prefix]_[name_city]_[tiles.index]_[name_building]_[distance to tile center.x]x[distance to tile center.y].fbx

=== Content ===

The origin of each model is at the center of its geometry and is positionned in the world.

Each model exported contains 2 UV maps:
* [0], ~global, contains global uvs, relative to the whole imported model
* [1], ~local, contains local uvs, relative to the model itself

=== Notes ===

* Script is heavy and therefore is slow, keep an eye on the terminal/console, it will warns you about its current status.
* If you run script several times, reload the blend file: some data are not correctly cleanup, blender will become slow.
* Generation of uv on ground will certainly be chaotic. Edit them manually to fix. Suggestion:
** in 3d window: top view
** perspective ortho
** project from view in global and local uvs

Important: execute in object mode!

Made with blender v2.78c

'''

name_city = "mons"
name_prefix = "ATVB"
name_obj_suffix = "obj"
name_mesh_suffix = "mesh"
name_ground = "ground"
name_road = "road"
name_building = "building"

import_folder = "C:\\Users\\frank\\Documents\\ATVB\\FZ-playground\\objs\\vectiler\\" + name_city + "\\"
export_folder = "C:\\Users\\frank\\Documents\\ATVB\\FZ-playground\\objs\\exports\\" + name_city + "\\"

global_scale = 100

enable_imports = True		# set to false if you work on a blend file already containing a vectiler import
enable_exports = True		# set to false to avoid fbx generation
enable_grounds = True		# enable grounds processing, does not prevent tiles and aabb computing
enable_roads = True			# enable roads processing
enable_buildings = True		# enable buildings processing

# tiles info - structure: [ tuple( index, pos x, pox y ), tuple( index, pos x, pox y ), etc ]
tiles = []

# working vars
# aabb: min x,y / max x,y / size x,y
aabb = [0,0,0,0,0,0]
print2void = io.StringIO()

#roof configuration
BORDER_MAX = 6
UP = mathutils.Vector((0,0,1))
FRONT = mathutils.Vector((0,1,0))
RIGHT = mathutils.Vector((1,0,0))

# material configuration
mat_ground = "ATVB_mat_ground"
mat_road = "ATVB_mat_road"
mat_walls = "ATVB_mat_walls"
mat_roof = "ATVB_mat_roof"

# console cleanup
msg_last_len = 0

def generate_mat( name, color ):
    
    if name in bpy.data.materials.keys():
        print( "material " + name + " exists" )
    else:
        mat = bpy.data.materials.new( name )
        mat.diffuse_color = color
        print( "material " + name + " created" )

def generate_mats():
    
    global mat_ground
    global mat_road
    global mat_walls
    global mat_roof
    
    generate_mat( mat_ground, ( 1,0,0 ) )
    generate_mat( mat_road, ( 0,1,0 ) )
    generate_mat( mat_walls, ( 1,0,1 ) )
    generate_mat( mat_roof, ( 0,1,1 ) )

def validate_obj( obj ):
	
	if len( obj.data.vertices ) < 6 or len(obj.data.polygons) < 5:
		obj.select = True
		bpy.ops.object.delete( use_global=False )
		return False
		
	return True

def roof( obj ):
	
	global BORDER_MAX 
	global UP
	global FRONT
	global RIGHT
	global print2void
	global mat_walls
	global mat_roof
	
	if validate_obj( obj ) == False:
		return False
	
	DIR_FRONT = mathutils.Vector((0,0,0))
	DIR_RIGHT = mathutils.Vector((0,0,0))
	TOP_CENTER = mathutils.Vector((0,0,0))
	BORDER_VERTICES_NUM = 0
	TOPZ = -1
	BOTTOMZ = obj.dimensions[2] * 0.5
	biggestfront = False
	
	bpy.context.scene.objects.active = obj
	
	bpy.ops.object.mode_set(mode='EDIT')
	with redirect_stdout(print2void):
		bpy.ops.mesh.remove_doubles( threshold=0.001, use_unselected=True )
	bpy.ops.object.mode_set(mode='OBJECT')
	
	if validate_obj( obj ) == False:
		return False
	
	for v in obj.data.vertices:
		v.select = False
	for e in obj.data.edges:
		e.select = False
	
	for f in obj.data.polygons:
		d = UP.dot( f.normal )
		if d > 0.99:
			f.select = True
			TOPZ = f.center[2]
		else:
			d = FRONT.dot( f.normal )
			if biggestfront == False or biggestfront < d:
				DIR_FRONT = f.normal.copy()
				biggestfront = d 
			f.select = False
	
	for v in obj.data.vertices:
		if v.co[2] == TOPZ:
			TOP_CENTER[0] += v.co[0]
			TOP_CENTER[1] += v.co[1]
			BORDER_VERTICES_NUM += 1
	
	if BORDER_VERTICES_NUM > BORDER_MAX:
		# adding material and finishing
		bpy.ops.object.material_slot_add()
		obj.material_slots[0].material = bpy.data.materials[ mat_walls ]
		return
	
	TOP_CENTER.normalize()
	
	bpy.ops.object.mode_set(mode='EDIT')
	with redirect_stdout(print2void):
		bpy.ops.mesh.delete(type='FACE')
	bpy.ops.object.mode_set(mode='OBJECT')
	
	DIR_RIGHT = DIR_FRONT.cross( UP )
	
	sizef = 0
	sizer = 0
	
	smallest_size = 0
	smallest_dir = 0
	
	for e in obj.data.edges:
		d0 = obj.data.vertices[ e.vertices[0] ].co[2] - TOPZ
		d1 = obj.data.vertices[ e.vertices[1] ].co[2] - TOPZ
		if abs( d0 ) < 1e-5 and abs( d1 ) < 1e-5:
			obj.data.vertices[ e.vertices[0] ].select == True
			obj.data.vertices[ e.vertices[1] ].select == True
			e.select = True
		else:
			e.select = False
		if e.select == True:
			v0 = obj.data.vertices[ e.vertices[0] ].co
			v1 = obj.data.vertices[ e.vertices[1] ].co
			dir = mathutils.Vector(( v1[0]-v0[0], v1[1]-v0[1], v1[2]-v0[2] ))
			dirlen = dir.length
			dir.normalize()
			df = DIR_FRONT.dot( dir )
			dr = DIR_RIGHT.dot( dir )
			#print( dir, e, df, dr )
			if df > 0 and abs( df - 1 ) < 1e-5:
				sizef = dirlen
			elif dr > 0 and abs( dr - 1 ) < 1e-5:
				sizer = dirlen
	
	front_switch = False
	smallest_size = sizef
	smallest_dir = DIR_FRONT
	other_dir = DIR_RIGHT
	scale = ( 0,1,1 )
	scale_axis = ( True, False, False )
	if smallest_size > sizer:
		front_switch = True
		smallest_size = sizer
		smallest_dir = DIR_RIGHT
		other_dir = DIR_FRONT
	
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.mesh.select_mode(type="VERT")
	
	bpy.ops.mesh.mark_seam(clear=False)
	
	# extrusion of top faces
	roof_height_min = 0.3
	roof_height_max = 0.6
	extrudz = obj.dimensions[2] * ( roof_height_min + random.random() * ( roof_height_max - roof_height_min ) )
	with redirect_stdout(print2void):
		bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_translate={"value":(0,0,extrudz), "constraint_axis":(False, False, False), "constraint_orientation":'GLOBAL', "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "gpencil_strokes":False, "texture_space":False, "remove_on_cancel":False, "release_confirm":False})
	
	# adding a face to generate correct normal
	bpy.ops.mesh.edge_face_add()
	with redirect_stdout(print2void):
		bpy.ops.mesh.normals_make_consistent(inside=False)
	bpy.ops.object.mode_set(mode='OBJECT')
	
	NEW_FRONT = mathutils.Vector((0,0,0))
	for f in obj.data.polygons:
		if f.select:
			
			if BORDER_VERTICES_NUM <= 4:
				seg0 = mathutils.Vector((
					obj.data.vertices[f.vertices[1]].co[0] - obj.data.vertices[f.vertices[0]].co[0],
					obj.data.vertices[f.vertices[1]].co[1] - obj.data.vertices[f.vertices[0]].co[1],
					obj.data.vertices[f.vertices[1]].co[2] - obj.data.vertices[f.vertices[0]].co[2] 
					)).normalized()
				seg1 = mathutils.Vector((
					obj.data.vertices[2].co[0] - obj.data.vertices[1].co[0],
					obj.data.vertices[2].co[1] - obj.data.vertices[1].co[1],
					obj.data.vertices[2].co[2] - obj.data.vertices[1].co[2]
					)).normalized()
				NEW_FRONT = seg0
			else:
				for vi in f.vertices:
					NEW_FRONT += obj.data.vertices[ vi ].normal
				NEW_FRONT.normalize()
			
			break
	
	bpy.ops.object.mode_set(mode='EDIT')
	with redirect_stdout(print2void):
		bpy.ops.mesh.quads_convert_to_tris(quad_method='BEAUTY', ngon_method='BEAUTY')
	bpy.ops.mesh.select_all(action='DESELECT')
	bpy.ops.object.mode_set(mode='OBJECT')
	
	# counting top vertices
	topvs = []
	topvs_opposite = []
	
	# selection of top vertices
	for v in obj.data.vertices:
		if v.co[2] > TOPZ:
			nv = mathutils.Vector( ( v.co[0] - TOP_CENTER[0], v.co[1] - TOP_CENTER[1], 0 ) )
			nv.normalize()
			if NEW_FRONT.dot( nv ) > 0:
				topvs.append( (v.co[0], v.co[1], v.co[2]) )
			else:
				topvs_opposite.append( (v.co[0], v.co[1], v.co[2]) )
	
	if len( topvs ) < len( topvs_opposite ):
		tmp = topvs_opposite
		topvs_opposite = topvs
		topvs = tmp
	
	for pv in topvs:
	
		v0 = False
		v0index = False
		i = 0
		for v in obj.data.vertices:
			if (
				pv[0] == v.co[0] and
				pv[1] == v.co[1] and
				pv[2] == v.co[2]
				):
					v0 = v
					v0index = i
					break
			i += 1
		
		if v0 == False:
			continue
		
		v1 = False
		smallest_d = 0
		opp_i = -1
		for e in obj.data.edges:
			o = -1
			if e.vertices[0] == v0index:
				o = e.vertices[1]
			elif e.vertices[1] == v0index:
				o = e.vertices[0]
			tmpo = obj.data.vertices[o]
			keepon = False
			oi = 0
			
			if len( topvs_opposite ):
				for t in topvs_opposite:
					if (
						t[0] == tmpo.co[0] and
						t[1] == tmpo.co[1] and
						t[2] == tmpo.co[2]
						):
						keepon = True
						break
					oi += 1
					
			elif tmpo.co[2] > TOPZ:
				keepon = True
				
			if keepon == False:
				continue
				
			nv = mathutils.Vector( ( tmpo.co[0] - v0.co[0], tmpo.co[1] - v0.co[1], 0 ) )
			nv.normalize()
			d = abs( NEW_FRONT.dot( nv ) )
			if v1 == False or smallest_d < d:
				v1 = tmpo
				smallest_d = d
				if len( topvs_opposite ):
					opp_i = oi
		
		if v1 == False:
			continue
		
		if  opp_i != -1:
			topvs_opposite = topvs_opposite[:opp_i] + topvs_opposite[opp_i+1:]
		
		v0.select = True
		v1.select = True
		
		bpy.ops.object.mode_set(mode='EDIT')
		with redirect_stdout(print2void):
			bpy.ops.mesh.merge(type='CENTER')
		bpy.ops.mesh.select_all(action='DESELECT')
		bpy.ops.object.mode_set(mode='OBJECT')
	
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.mesh.select_all(action='SELECT')
	with redirect_stdout(print2void):
		#cleanup
		bpy.ops.mesh.remove_doubles()
	with redirect_stdout(print2void):
		#fix normals
		bpy.ops.mesh.normals_make_consistent(inside=False)
	bpy.ops.mesh.select_all(action='DESELECT')
	bpy.ops.object.mode_set(mode='OBJECT')
	
	# generation of seams
	for e in obj.data.edges:
		v0 = obj.data.vertices[ e.vertices[0] ]
		v1 = obj.data.vertices[ e.vertices[1] ]
		if v0.co[2] > TOPZ or v1.co[2] > TOPZ:
			e.select = True
		else:
			e.select = False
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.mesh.mark_seam(clear=False)
	bpy.ops.object.mode_set(mode='OBJECT')
	
	# generation of vertex groups
	# walls is index 0
	bpy.ops.object.vertex_group_add()
	obj.vertex_groups[ obj.vertex_groups.active_index ].name = 'walls'
	bpy.ops.object.vertex_group_set_active(group='walls')
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.object.vertex_group_select()
	bpy.ops.mesh.select_all(action='DESELECT')
	bpy.ops.object.mode_set(mode='OBJECT')
	for f in obj.data.polygons:
		if f.center[2] <= TOPZ:
			for vi in f.vertices:
				obj.data.vertices[vi].select = True
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.object.vertex_group_assign()
	bpy.ops.object.mode_set(mode='OBJECT')
	
	# roof is index 0
	bpy.ops.object.vertex_group_add()
	obj.vertex_groups[ obj.vertex_groups.active_index ].name = 'roof'
	bpy.ops.object.vertex_group_set_active(group='roof')
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.object.vertex_group_select()
	bpy.ops.mesh.select_all(action='DESELECT')
	bpy.ops.object.mode_set(mode='OBJECT')
	for f in obj.data.polygons:
		if f.center[2] >= TOPZ:
			for vi in f.vertices:
				obj.data.vertices[vi].select = True
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.object.vertex_group_assign()
	bpy.ops.object.mode_set(mode='OBJECT')
	
	# materials assignation
	bpy.ops.object.material_slot_add()
	bpy.ops.object.material_slot_add()
	obj.material_slots[0].material = bpy.data.materials[ mat_walls ]
	obj.material_slots[1].material = bpy.data.materials[ mat_roof ]
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.mesh.select_all(action='DESELECT')
	bpy.context.object.active_material_index = 0
	bpy.context.object.vertex_groups.active_index = 0
	bpy.ops.object.vertex_group_select()
	bpy.ops.object.material_slot_assign()
	bpy.ops.mesh.select_all(action='DESELECT')
	bpy.context.object.active_material_index = 1
	bpy.context.object.vertex_groups.active_index = 1
	bpy.ops.object.vertex_group_select()
	bpy.ops.object.material_slot_assign()
	bpy.ops.object.mode_set(mode='OBJECT')
	
	# finishing by moving the origin to the floor of the model
	bpy.context.scene.cursor_location = (obj.location[0], obj.location[1], obj.location[2] - BOTTOMZ )
	bpy.ops.object.origin_set(type='ORIGIN_CURSOR')
	
	return True

def msg(txt):
	
	global msg_last_len
	
	tl = len( txt )
	if tl < msg_last_len:
		diff = msg_last_len - tl
		while diff > 0:
			txt = txt + " "
			diff -= 1
	
	sys.stdout.write(chr(13))
	sys.stdout.flush()
	sys.stdout.write(txt + chr(13))
	sys.stdout.flush()
	
	# console have been cleanup anyway
	msg_last_len = tl

def get_tile( obj ):
	
	global tiles
	
	# closest tile, for index
	tile = False
	tile_closest = 0
	for t in tiles:
		d = math.sqrt((t[1] - obj.location[0])**2 + (t[2] - obj.location[1])**2)
		if tile == False or tile_closest > d:
			tile = t
			tile_closest = d
	return tile

def namestart( obj, s ):

	global name_prefix
	global name_city
	global tiles
	
	t = get_tile( obj )
	return name_prefix + "_" + name_city + "_" + str( num_leading_zero( t[0], 3 ) ) + "_" + s

def num_leading_zero( num, digits ):
	neg = False
	if num < 0:
		neg = True
	s = str( abs( num ) )
	while len( s ) < digits:
		s = "0" + s
	if neg == True:
		s = "-" + s
	return s

def pos_to_string( obj, precision ):
	
	tile = get_tile( obj )
	digits = len( str( precision ) )
	deltax = obj.location[0] - tile[1]
	deltay = obj.location[1] - tile[1]
	return "_" + num_leading_zero( round( deltax * precision ), digits ) + "x" + num_leading_zero( round( deltay * precision ), digits  )

def merge_clean_split( merge = True ):
	# not used: too heavy!
	print( "**** merge_clean_split starts on " + bpy.context.scene.objects.active.name )
	bpy.ops.object.join()
	for o in bpy.data.objects:
		if o.select == True:
			bpy.context.scene.objects.active = o
			break
	print( "**** adding uv slot" )
	bpy.ops.mesh.uv_texture_add()
	bpy.context.scene.objects.active.data.uv_textures[0].name = 'global'
	bpy.context.scene.objects.active.data.uv_textures[0].active = True
	print( "**** selecting all vertices")
	for vert in bpy.context.scene.objects.active.data.vertices:
		vert.select = True
	print( "**** edit mode" )
	bpy.ops.object.mode_set(mode='EDIT')
	if merge == True:
		print( "**** remove doubles")
		bpy.ops.mesh.remove_doubles(threshold=0.01)
	print( "**** uv unwrap" )
	bpy.ops.uv.smart_project()
	print( "****separation" )
	bpy.ops.mesh.separate(type='LOOSE')
	bpy.ops.object.mode_set(mode='OBJECT')
	print( "**** merge_clean_split finished on " + bpy.context.scene.objects.active.name )

def generate_uv( obj ):
	
	global aabb
	global print2void
	if validate_obj( obj ) == False:
		return False
	
	bpy.context.scene.objects.active = obj
	bpy.ops.object.shade_flat()
	
	# generation of world UV
	bpy.ops.mesh.uv_texture_add()
	uvid = 0
	scalex = obj.dimensions[0] / aabb[4]
	scaley = obj.dimensions[1] / aabb[5]
	relx = ( ( obj.location[0] - obj.dimensions[0] * 0.5 ) - aabb[0] ) / aabb[4]
	rely = ( ( obj.location[1] - obj.dimensions[1] * 0.5 ) - aabb[1] ) / aabb[5]
	prevdata = obj.data.uv_layers[uvid].data
	uvid = len( bpy.context.scene.objects.active.data.uv_textures )
	bpy.ops.mesh.uv_texture_add()
	obj.data.uv_textures[uvid].name = 'global'
	obj.data.uv_textures[uvid].active = True
	
	for vert in obj.data.vertices:
		vert.select = True
	
	bpy.ops.object.mode_set(mode='EDIT')
	with redirect_stdout(print2void):
		bpy.ops.uv.smart_project()
	bpy.ops.object.mode_set(mode='OBJECT')
	
	l = len( obj.data.uv_layers[uvid].data )
	for i in obj.data.uv_layers[uvid].data:
		i.uv[0] = relx + ( i.uv[0] * scalex)
		i.uv[1] = rely + ( i.uv[1] * scaley)

def generate_uvs( obj ):
	
	global aabb
	global print2void
	
	if validate_obj( obj ) == False:
		return False
	
	bpy.context.scene.objects.active = obj
	bpy.ops.object.shade_flat()
	
	# generation of standard UV
	uvid = len( bpy.context.scene.objects.active.data.uv_textures )
	bpy.ops.mesh.uv_texture_add()
	obj.data.uv_textures[uvid].name = 'local'
	obj.data.uv_textures[uvid].active = True
	
	for vert in obj.data.vertices:
		vert.select = True
	
	bpy.ops.object.mode_set(mode='EDIT')
	with redirect_stdout(print2void):
		bpy.ops.uv.smart_project()
	bpy.ops.uv.select_all(action='DESELECT')
	bpy.ops.object.mode_set(mode='OBJECT')
		
	# generation of world UV
	scalex = obj.dimensions[0] / aabb[4]
	scaley = obj.dimensions[1] / aabb[5]
	relx = ( ( obj.location[0] - obj.dimensions[0] * 0.5 ) - aabb[0] ) / aabb[4]
	rely = ( ( obj.location[1] - obj.dimensions[1] * 0.5 ) - aabb[1] ) / aabb[5]
	prevdata = obj.data.uv_layers[uvid].data
	uvid = len( bpy.context.scene.objects.active.data.uv_textures )
	bpy.ops.mesh.uv_texture_add()
	obj.data.uv_textures[uvid].name = 'global'
	obj.data.uv_textures[uvid].active = True
	
	bpy.ops.object.mode_set(mode='EDIT')
	with redirect_stdout(print2void):
		bpy.ops.uv.smart_project()
	bpy.ops.object.mode_set(mode='OBJECT')
	
	l = len( obj.data.uv_layers[uvid].data )
	for i in obj.data.uv_layers[uvid].data:
		i.uv[0] = relx + ( i.uv[0] * scalex)
		i.uv[1] = rely + ( i.uv[1] * scaley)
	
	# switching to local uv
	obj.data.uv_textures.active_index = 0
	obj.data.uv_layers.active_index = 0
	
	return True

def ground_process( obj ):

	global name_obj_suffix
	global name_mesh_suffix
	global name_ground
	global mat_ground
	
	generate_uv(obj)
	
	# material assignation
	if len( obj.material_slots ) == 0:
		bpy.ops.object.material_slot_add()
	obj.material_slots[0].material = bpy.data.materials[ mat_ground ]
	
	# position suffix
	posstr = pos_to_string( obj, 1 )
	
	name_prefix = namestart( obj, name_ground )
	
	# object
	if len( name_obj_suffix ) > 0:
		obj.name = name_prefix + "_" + name_obj_suffix
	obj.name = obj.name + posstr
	
	# mesh
	obj.data.name = namestart( obj, name_ground )
	if len( name_mesh_suffix ) > 0:
		obj.name = name_prefix + "_" +  name_mesh_suffix
	obj.data.name = obj.data.name + posstr

def road_process( obj ):

	global name_obj_suffix
	global name_mesh_suffix
	global name_road
	global mat_road
	
	if generate_uvs(obj) == False:
		return False
	
	bpy.context.scene.objects.active = obj
	
	# material assignation
	if len( obj.material_slots ) == 0:
		bpy.ops.object.material_slot_add()
	obj.material_slots[0].material = bpy.data.materials[ mat_road ]
	
	name_prefix = namestart( obj, name_road )
	
	# position suffix
	posstr = pos_to_string( obj, 100 )
	# object
	if len( name_obj_suffix ) > 0:
		obj.name = name_prefix + "_" + name_obj_suffix
	obj.name = obj.name + posstr
	# mesh
	if len( name_mesh_suffix ) > 0:
		obj.data.name = name_prefix + "_" + name_mesh_suffix
	obj.data.name = obj.data.name + posstr
	
	return True

def building_process( obj ):

	global name_obj_suffix
	global name_mesh_suffix
	global name_building
	global name_building
	
	if roof(obj) == False:
		return False
	
	if generate_uvs(obj) == False:
		return False
	
	name_prefix = namestart( obj, name_building )
	
	# position suffix
	posstr = pos_to_string( obj, 1000 )
	# object
	if len( name_obj_suffix ) > 0:
		obj.name = name_prefix + "_" + name_obj_suffix
	obj.name = obj.name + posstr
	# mesh
	if len( name_mesh_suffix ) > 0:
		obj.data.name = name_prefix + "_" + name_mesh_suffix
	obj.data.name = obj.data.name + posstr
	
	return True

def cleanup( obj ):
	
	global global_scale
	
	bpy.ops.object.select_all(action='DESELECT')
	
	bpy.context.scene.objects.active = obj
	obj.select = True
	obj.rotation_euler = (0,0,0)
	obj.scale = ( global_scale,global_scale,global_scale )
	bpy.ops.object.transform_apply(scale=True)
	bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY')
	
	bpy.ops.object.select_all(action='DESELECT')

def load_roads():

	global import_folder
	global tiles
	global aabb
	global enable_roads
	
	print( "** processing roads **" )
	
	if enable_imports == True:
		roads = os.path.join(import_folder, 'roads.obj')
		bpy.ops.import_scene.obj(filepath=roads)
	
	initaabb = True
	gnames = []
	rnames = []
	gindex = 0
	for obj in bpy.data.objects:
		if obj.name.startswith( 'mesh' ):
			dimx = obj.dimensions[0]
			dimy = obj.dimensions[1]
			if dimx == 2.0 and dimy == 2.0:
				cleanup( obj )
				gnames.append( obj.name )
				minx = obj.location[0] - dimx * 0.5
				miny = obj.location[1] - dimy * 0.5
				maxx = obj.location[0] + dimx * 0.5
				maxy = obj.location[1] + dimy * 0.5
				if initaabb == True:
					aabb[0] = minx
					aabb[1] = miny
					aabb[2] = maxx
					aabb[3] = maxy
					initaabb = False
				else:
					if aabb[0] > minx:
						aabb[0] = minx
					if aabb[1] > miny:
						aabb[1] = miny
					if aabb[2] < maxx:
						aabb[2] = maxx
					if aabb[3] < maxy:
						aabb[3] = maxy
				tiles.append( ( gindex, obj.location[0], obj.location[1] ) )
				gindex += 1
			else:
				rnames.append( obj.name )
	
	# finish aabb
	aabb[4] = aabb[2] - aabb[0]
	aabb[5] = aabb[3] - aabb[1]
	
	totalnum = len( gnames )
	currnum = 1
	for n in gnames:
		obj = bpy.data.objects[ n ]
		bpy.ops.object.select_all(action='DESELECT')
		if enable_grounds == True:
			obj.select = True
			ground_process( obj )
			msg( "processing grounds, ground '" + n + "' as '" + obj.name + "', " + str( currnum ) + "/" + str( totalnum ) )
			obj.select = False
		else:
			obj.select = True
			bpy.ops.object.delete( use_global=False )
			msg( "delete ground '" + n + "'" )
		currnum += 1
	
	totalnum = len( rnames )
	currnum = 1
	for n in rnames:
		obj = bpy.data.objects[ n ]
		bpy.ops.object.select_all(action='DESELECT')
		if enable_roads == True:
			obj.select = True
			cleanup( obj )
			if road_process( obj ) == True:
				msg( "processing roads, road '" + n + "' as '" + obj.name + "', " + str( currnum ) + "/" + str( totalnum ) )
				obj.select = False
			else:
				bpy.ops.object.delete( use_global=False )
		else:
			obj.select = True
			bpy.ops.object.delete( use_global=False )
			msg( "delete road '" + n + "'" )
		currnum += 1
	
	print( "*** bounding box:", aabb )
	
	print( "** buildings roads **" )

def load_buildings():
	
	global import_folder
	global enable_buildings
	
	print( "** processing buildings **" )
	
	if enable_imports == True:
		print( "*** importing buildings ***" )
		buildings = os.path.join(import_folder, 'buildings.obj')
		bpy.ops.import_scene.obj(filepath=buildings)
		
	#removing grounds
	print( "*** grounds removal ***" )
	buildings_toprocess = []
	bpy.ops.object.select_all(action='DESELECT')
	for obj in bpy.data.objects:
		if obj.name.startswith( 'mesh' ):
			dimx = obj.dimensions[0]
			dimy = obj.dimensions[1]
			if dimx == 2.0 and dimy == 2.0:
				obj.select = True
			else:
				buildings_toprocess.append( obj.name )
	bpy.ops.object.delete(use_global=False)
	
	if enable_buildings == True:
		print( "*** individual process ***" )
		l = len( buildings_toprocess )
		buildings_processed = 1
		for pn in buildings_toprocess:
			obj = bpy.data.objects[ pn ]
			msg( "**** processing '" + pn + "'" )
			bpy.ops.object.select_all(action='DESELECT')
			obj.select = True
			cleanup( obj )
			if  building_process( obj ) == True:
				msg( "**** success: '" + obj.name + "', " + str( buildings_processed ) + "/" + str( l ) )
				obj.select = False
			else:
				msg( "**** failed: '" + pn + "', " + str( buildings_processed ) + "/" + str( l ) )
			buildings_processed += 1
			
	print( "** buildings done **" )

def export_all():
	
	global export_folder
	global print2void
	
	print( "** export **" )
	
	if not os.path.exists(export_folder):
		os.makedirs(export_folder)
		print( "**** creation of '" + export_folder + "'" )
	
	toprocess = len( bpy.data.objects )
	processed = 1
	bpy.ops.object.select_all(action='DESELECT')
	for obj in bpy.data.objects:
		bpy.context.scene.objects.active = obj
		obj.select = True
		bpy.ops.object.shade_flat()
		pn = obj.name
		path = os.path.join(export_folder, pn + ".fbx" )
		with redirect_stdout(print2void):
			bpy.ops.export_scene.fbx( filepath=path, use_selection=True )
		msg( "**** object '" + pn + "' exported to '" + path + "', " + str( processed ) + "/" + str( toprocess ) )
		obj.select = False
		processed += 1
	
	print( "** export done **" )

#***************** MAIN *****************

start_time = time.clock()

if enable_imports == True:
	bpy.ops.object.select_all(action='SELECT')
	bpy.ops.object.delete(use_global=False)

generate_mats()

load_roads()
load_buildings()

if enable_exports == True:
	export_all()

print( "process execution time:",  time.clock() - start_time, "seconds" )
#print( bpy.data.objects )