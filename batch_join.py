import bpy

'''
join object by name, string before the marker is considered as relevant
execute in object mode!
'''

marker = ".obj"
current = ""

bpy.ops.object.select_all(action='DESELECT')

sorted_names = []
for obj in bpy.context.scene.objects:
    sorted_names.append( obj.name )
sorted_names.sort()

print( sorted_names )

for obj_name in sorted_names:
    obj = bpy.data.objects[obj_name]
    mpos = obj_name.index( marker )
    prefix = obj_name[:mpos]
    if current != prefix:
        bpy.ops.object.join()
        bpy.ops.object.select_all(action='DESELECT')
        obj.select = True
        bpy.context.scene.objects.active = obj
        current = prefix
        print("current: " + prefix )
    else:
        obj.select = True

if current != "":
    bpy.ops.object.join()
    bpy.ops.object.select_all(action='DESELECT')
