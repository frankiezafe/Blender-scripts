import bpy, random

def test():
    print( "random_actions loaded" )

def random_extrude( min, max ):
    # MESH_OT_extrude_region
    ot = {"use_normal_flip":False, "mirror":False}
    # TRANSFORM_OT_shrink_fatten
    tr = { "value": min + random.random() * (max-min) }
    bpy.ops.mesh.extrude_region_shrink_fatten( MESH_OT_extrude_region=ot, TRANSFORM_OT_shrink_fatten = tr)

def random_move( min, max ):
    value = (0,0,min + random.random() * (max-min))
    bpy.ops.transform.translate(value=value, orient_type='NORMAL',orient_matrix_type='NORMAL', constraint_axis=(False, False, False))

def random_rotate( mmx, mmy, mmz ):
    value = mmz[0] + random.random() * (mmz[1]-mmz[0])
    bpy.ops.transform.rotate(value=value, orient_axis='Z', orient_type='NORMAL',orient_matrix_type='NORMAL')
    value = mmx[0] + random.random() * (mmx[1]-mmx[0])
    bpy.ops.transform.rotate(value=value, orient_axis='X', orient_type='NORMAL',orient_matrix_type='NORMAL')
    value = mmy[0] + random.random() * (mmy[1]-mmy[0])
    bpy.ops.transform.rotate(value=value, orient_axis='Y', orient_type='NORMAL',orient_matrix_type='NORMAL')

def random_resize( mmx, mmy, mmz ):
    value=(mmx[0] + random.random() * (mmx[1]-mmx[0]), mmy[0] + random.random() * (mmy[1]-mmy[0]), mmz[0] + random.random() * (mmz[1]-mmz[0]))
    bpy.ops.transform.resize(value=value, orient_type='NORMAL', orient_matrix_type='NORMAL')

def random_inset( min, max ):
    value = min + random.random() * (max-min)
    bpy.ops.mesh.inset( thickness=value, depth=0)