import bpy

bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.join()

for o in bpy.data.objects:
    if o.select == True:
        bpy.context.scene.objects.active = o
        break

bpy.ops.mesh.uv_texture_add()
bpy.context.scene.objects.active.data.uv_textures[0].name = 'global'
bpy.ops.uv.smart_project()

bpy.ops.object.mode_set(mode='EDIT')        
bpy.ops.mesh.separate(type='LOOSE')
bpy.ops.object.mode_set(mode='OBJECT')