import bpy
import os
import math

#### GLOBALS ####

# nombre maximum de fichiers à importer
MAX_FILES = -1
# disposition en grille
GRID_DISPLAY = True
# automatic UV unwrapping
GENERATE_UV = False # !!! ralentit l'importation à mort !!!
# material
MATERIAL = 'uv_display'
# dossier à scanner
ASSETS_FOLDER = bpy.path.abspath("//assets_additivist")
# liste des formats reconnus et commande d'importation
FORMATS = {
    'STL': { 
        'ext': [ '.stl' ], 
        'command':'bpy.ops.import_mesh.stl(filepath=\'##PATH##\')' 
    },
    'COLLADA': { 
        'ext': [ '.dae' ],
        'command':'bpy.ops.wm.collada_import(filepath=\'##PATH##\')' 
    },
    'OBJ': { 
        'ext': [ '.obj' ],
        'command':'bpy.ops.import_scene.obj(filepath=\'##PATH##\')' 
    },
    'PLY': { 
        'ext': [ '.ply' ],
        'command':'bpy.ops.import_mesh.ply(filepath=\'##PATH##\')' 
    },
    'BLEND': { 
        'ext': [ '.blend' ],
        'command':'bpy.data.libraries.load(filepath=\'##PATH##\')' 
    }
}

#### PROCESS ####

# nettoyage de la scene courante
try:
    bpy.ops.object.mode_set( mode='OBJECT' )
except:
    # already good
    pass
bpy.ops.object.select_all( action = 'SELECT' )
bpy.ops.object.delete()

# collecte des fichiers à importer
files = []

for (dirpath, dirnames, filenames) in os.walk(ASSETS_FOLDER):
    
    #print(dirpath)
    #print(dirnames)
    #print(filenames)
   
    for filename in filenames:
        
        # extraction de l'extension du fichier
        name, ext = os.path.splitext(filename)
        ext = ext.lower()
        
        # recherche dans la liste de formats reconnus
        found = False
        for format in FORMATS:
            for e in FORMATS[ format ][ 'ext' ]:
                if ext == e:
                    found = True
                    f = {}
                    f['format'] = format
                    f['name'] = name
                    f['path'] = os.path.join( dirpath, filename )
                    files.append( f )
                    break

files = sorted(files, key=lambda k: k['name'])

# affichage des fichiers reconnus
print( str( len( files ) ) + " fichier(s) à importer" )

# cursor at world origin
bpy.context.scene.cursor.location = [0,0,0]
# importation
imported = 0
for f in files:
    
    if MAX_FILES > -1 and imported >= MAX_FILES:
        break
    
    print( 'start importation of ', f['name'], ' (', f['format'], ')' )
    
    path = f['path']
    # creation de la commande d'importation du fichier
    cmd = FORMATS[ f['format'] ][ 'command' ].replace( '##PATH##', path )
    # print( cmd )
    
    try:
        if f['format'] is 'BLEND':
            with bpy.data.libraries.load(path) as (data_from, data_to):
                print( data_from.objects )
                data_to.objects = data_from.objects
                bpy.context.scene.update()
                for ob in data_to.objects:
                    bpy.context.scene.collection.objects.link( bpy.data.objects[ ob ] )
        else:
            exec( cmd )
    except:
        print( f['name'], ' >> boom...' )
        pass
    
    imported += 1

objects = []
bpy.ops.object.select_all( action = 'SELECT' )
for o in bpy.context.selected_objects:
    objects.append( o )

grid_size = -1
if GRID_DISPLAY:
    grid_size = math.ceil( math.sqrt( imported ) )
    print( grid_size )

# loading material
mat = None
try:
    mat = bpy.data.materials.get( MATERIAL )
except:
    print( 'no material', MATERIAL, 'found' )
    pass

num = 0
column = 0
row = 0
for o in objects:
    
    is_mesh = True
    try:
        o.data.polygons
    except:
        is_mesh = False 
    
    print( 'adjusting', o.name )
    
    # selecting object
    bpy.ops.object.select_all( action = 'DESELECT' )
    #bpy.ops.object.select_pattern( pattern = o.name )
    bpy.context.view_layer.objects.active = o
    o.select_set(True)
    
    # linking to current collection
    #bpy.context.scene.collection.objects.link( o )
    
    # scaling
    if is_mesh:
        dim = o.dimensions
        largest = 0
        for i in range(0,3):
            if dim[i] > largest:
                largest = dim[i]
        scaling = 1.0 / largest
        o.scale = [ scaling, scaling, scaling ]
        
        # applying modifications
        bpy.ops.object.transform_apply( location=True, rotation=True, scale=True )
        
        print( 'scaling', o.name )
        
    # adjusting center
    bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='MEDIAN')
    o.location = [ 0, 0, 0 ]
    
    
    if is_mesh and GENERATE_UV:
        
        print( 'unwrapping', o.name )
        
        bpy.ops.object.mode_set( mode ='EDIT' )
        bpy.ops.uv.smart_project()
        bpy.ops.object.mode_set( mode ='OBJECT' )
        
        if mat != None:
            # Assign it to object
            if o.data.materials:
                # assign to 1st material slot
                o.data.materials[0] = mat
            else:
                # no slots
                o.data.materials.append(mat)
    
    if GRID_DISPLAY:
        o.location = [ ( column - ((grid_size-1)*0.5) ) * 1.5, ( row - ((grid_size-1)*0.5) ) * 1.5, 0 ]
        column += 1
        if column >= grid_size:
            column = 0
            row += 1