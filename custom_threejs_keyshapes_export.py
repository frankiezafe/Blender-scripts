'''
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)
 
 Copyright (c) 2018 françois zajéga
 All rights reserved. 
 
 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software 
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)

'''

'''

Javasript to load result of this export:

function json_load_success( e ) {

	try{ 

		var data = JSON.parse( e );

		console.log( data );

		if ( data.vertices === undefined ) {
			console.error( "PantonPouf, no vertices in '" + this.config.path + "'" );
			return;
		} else if ( data.vertices.length == 0 ) {
			console.error( "PantonPouf, no vertices in '" + this.config.path + "'" );
			return;
		}

		function disposeArray() { this.array = null; }
		var buffgeom = new THREE.BufferGeometry();
		buffgeom.setIndex( data.indices );
		buffgeom.addAttribute( 'position', new THREE.Float32BufferAttribute( data.vertices, 3 ).onUpload( disposeArray ) );
		if ( data.normals !== undefined && data.normals.length != 0 ) {
			buffgeom.addAttribute( 'normal', new THREE.Float32BufferAttribute( data.normals, 3 ).onUpload( disposeArray ) );
		}
		if ( data.colors !== undefined && data.colors.length != 0 ) {
			buffgeom.addAttribute( 'color', new THREE.Float32BufferAttribute( data.colors, 3 ).onUpload( disposeArray ) );
		}
		if ( data.uvs !== undefined && data.uvs.length != 0 ) {
			buffgeom.addAttribute( 'uv', new THREE.Float32BufferAttribute( data.uvs, 2 ).onUpload( disposeArray ) );
		}
		buffgeom.computeBoundingSphere();

		var geom = new THREE.Geometry();
		geom.fromBufferGeometry( buffgeom );

		var has_morphs = false;
		var has_vnormals = false;

		for( var k in data.morphs ) {

			var vs = data.morphs[k].vertices;
			var fn = data.morphs[k].face_normals;
			var vn = data.morphs[k].vertex_normals;
			var l = vs.length;
			var i = 0;

			has_morphs = true;
			has_vnormals = ( vn !== undefined && vn.length == data.vertices.length );

			var frame = {
				name: k,
				vertices: []
			};

			if ( has_vnormals ) {
				frame.normals = [];
			}

			while ( i < l ) {
				frame.vertices.push( new THREE.Vector3( vs[i+0], vs[i+1], vs[i+2] ) );
				if ( has_vnormals ) {
					frame.normals.push( new THREE.Vector3( vn[i+0], vn[i+1], vn[i+2] ) );
				}
				i += 3;
			}

			geom.morphTargets.push( frame );

			this.morph_weights[ k ] = Interpolation.new_interpolation( 
				k,
				this.config.weight_init,
				this.config.weight_speed,
				this.config.weight_method
			);
			if ( k == this.config.default_morph ) {
				this.morph_weights[ k ].set( 1 );
			}

		}

		if ( has_vnormals ) {

			geom.vertices = geom.morphTargets[ 0 ].vertices;
			var morphTarget = geom.morphTargets[ 0 ];
			for ( var j = 0, jl = geom.faces.length; j < jl; j ++ ) {
				var face = geom.faces[ j ];
				face.vertexNormals = [
					morphTarget.normals[ face.a ],
					morphTarget.normals[ face.b ],
					morphTarget.normals[ face.c ]
				];
			}

			for ( var i = 0, l = geom.morphTargets.length; i < l; i ++ ) {
				var morphTarget = geom.morphTargets[ i ];
				var vertexNormals = [];
				for ( var j = 0, jl = geom.faces.length; j < jl; j ++ ) {
					var face = geom.faces[ j ];
					vertexNormals.push( {
						a: morphTarget.normals[ face.a ],
						b: morphTarget.normals[ face.b ],
						c: morphTarget.normals[ face.c ]
					} );
				}
				geom.morphNormals.push( { vertexNormals: vertexNormals } );
			}
		}

		// pushing 
		// var material = new THREE.MeshLambertMaterial( { 
		var material = new THREE.MeshPhongMaterial( { 
				color: 0xffffff, 
				shininess: 100,
				morphTargets: has_morphs, 
				morphNormals: has_vnormals } );

		mesh = new THREE.Mesh( geom, material );
		scene.add( mesh );

		var helper = new THREE.VertexNormalsHelper( mesh, 0.5, 0x00ffff, 1 );
		scene.add( helper );

	} catch( exception ) {

		console.log( "parsing of json failed, \n" + exception );

	}

};

'''

import bpy
import math
import mathutils

# obect name
export_obj = "pouf_export"
# name your uv correctly in mesh tab!
export_mesh = "pouf_export"
# export path
export_path = "/home/frankiezafe/projects/sisyphus/public/models/pouf.json"
# turn this on to remove tabs and line breaks in json
minimize_json = False
# turn this off for sharp edges everywhere - if turned on, export takes 'sharp edges' into account
smooth_normals = True

'''
global vars
'''

face_based_enabled = True
mesh = bpy.data.meshes[export_mesh]

all_indices = ""
all_pos = ""
all_norms = ""
all_colors = ""
all_uvs = ""
all_keyshapes = []

correction_pos = mathutils.Matrix.Identity(4)
correction_pos = bpy.data.objects[export_obj].matrix_world * correction_pos
axis_mat = mathutils.Matrix.Identity(4)
axis_mat[0] = [1,0,0,0]
axis_mat[1] = [0,0,1,0]
axis_mat[2] = [0,-1,0,0]
correction_pos = axis_mat * correction_pos

correction_normal = axis_mat.copy()

log = ""

'''
collecting data
'''

vertices = mesh.vertices
vcount = 0
ncount = 0
ccount = 0
faces = mesh.polygons
edges = []
uvlayer = mesh.uv_layers[0]
loops = mesh.loops
uvcount = 0
shape_keys = mesh.shape_keys.key_blocks

export_faces = []

class ExportVertice():

	def __init__( self, vid, face, force_sharp = False ):

		global edges

		self.id = vid
		self.default_face = face
		self.normal = mathutils.Vector((0,0,0))
		self.connected_edges = []
		self.connected_faces = []
		self.sharp = force_sharp

		if force_sharp:
			return

		i = 0
		l = len( edges )
		while i < l:
			e = edges[i]
			i += 1
			if e.vertices[0] == self.id or e.vertices[1] == self.id:
				self.connected_edges.append( e )
				if e.use_edge_sharp:
					self.sharp = True

	def connect_to_faces( self, efaces ):

		if self.sharp:
			return

		for e in self.connected_edges:
			for f in efaces:
				if f.is_connected_to_edge( e ):
					self.connected_faces.append( f )

	def compute_normal( self ):

		if self.sharp or len(self.connected_faces) <= 1:
			self.normal = self.default_face.normal
			return

		self.normal = mathutils.Vector((0,0,0))
		for f in self.connected_faces:
			self.normal += f.normal

		self.normal.normalize()


class ExportEdge():

	def __init__( self, vid0, vid1 ):

		self.vertices = []
		self.vertices.append( vid0 )
		self.vertices.append( vid1 )
		self.use_edge_sharp = False


class ExportFace( ExportVertice, dict ) :

	def __init__(self):

		self.verts = []
		self.edges = []
		self.normal = 0

	def is_connected_to_edge( self, e ):

		e0  = (e == self.edges[0] )
		e1  = (e == self.edges[1] )
		e2  = (e == self.edges[2] )
		return e0 or e1 or e2

	def share_edge( self, other ):

		if other is self:
			return True

		for e in self.edges:
			for oe in other.edges:
				if oe is e and not e.use_edge_sharp:
					return True

		return False

def to_string_vec3( v ):

	return str(v.x) + "," + str(v.y) + "," + str(v.z)

def to_string_co( v ):

	if len( v ) == 2:
		return str(v[0]) + "," + str(v[1])

	elif len( v ) == 3:
		return str(v[0]) + "," + str(v[1]) + "," + str(v[2])

def apply_mat_pos( co ):

	global correction_pos

	mat = mathutils.Matrix.Translation((co[0],co[1],co[2]))
	mat = correction_pos * mat
	return mat.to_translation()

def apply_mat_normal( co ):

	global correction_normal
	mat = mathutils.Matrix.Translation((co[0],co[1],co[2]))
	mat = correction_normal * mat
	return to_string_vec3(mat.to_translation())

def apply_mat( co ):

	return to_string_vec3(apply_mat_pos(co))

def add_vertex( vid, pos = True, norm = True, col = False ):

	global vertices
	vert = vertices[vid]

	if pos:
		add_pos( apply_mat( vert.co ) )
	if norm:
		add_pos( apply_mat( vert.normal ) )
	if col:
		add_color( apply_mat( vert.normal ) )

def add_pos( v ):

	global all_pos
	global vcount

	if len(all_pos) != 0:
		all_pos += ","
	all_pos += to_string_vec3( v )
	vcount += 1

def add_normal( v ):

	global all_norms
	global ncount

	if len(all_norms) != 0:
		all_norms += ","
	all_norms += to_string_vec3( v )
	ncount += 1

def add_color( v ):

	global all_colors
	global ccount

	if len(all_colors) != 0:
		all_colors += ","
	all_colors += to_string_vec3( v )
	ccount += 1

def add_uv( vid ):

	global all_uvs
	global uvlayer
	global uvcount

	if len(all_uvs) != 0:
		all_uvs += ", "
	all_uvs += to_string_co( uvlayer.data[vid].uv )
	uvcount += 1

def add_custom_vertex( v ):

	global all_pos
	global vcount

	if len(all_pos) != 0:
		all_pos += ","
	all_pos += to_string_co( v )
	vcount += 1

def compute_normal( v0, v1, v2 ):

	side_a = v0 - v1
	side_a.normalize()
	side_b = v0 - v2
	side_b.normalize()
	norm = side_a.cross( side_b )
	norm.normalize()
	return norm

def add_computed_normal( n ):

	global all_norms
	global ncount

	if len(all_norms) != 0:
		all_norms += ","
	all_norms += to_string_co( n )
	ncount += 1

def new_keyshape(name):

	return { 'name':name, 'vcount':0, 'vncount':0, 'vertices':'', 'vertex_normals':'' }

def check_edges( face ):

	global edges
	global faces
	global loops

	vnum = len(face.vertices)

	for i in range(0,vnum):
		e = edges[ loops[ face.loop_start+i ].edge_index ]
		if e.use_edge_sharp:
			print( "face touch a sharp edge @", loops[ face.loop_start+i ].edge_index )

def populate_edges():

	for e in mesh.edges:
		edges.append( e )

def retrieve_edges( face, vid0, vid1 ):

	global edges
	global faces
	global loops

	if len(edges) == 0:

		populate_edges()

	vnum = len(face.vertices)

	for i in range(0,vnum):
		e = edges[ loops[ face.loop_start+i ].edge_index ]
		vs = e.vertices
		if vs[0] == vid0 and vs[1] == vid1 or vs[1] == vid0 and vs[0] == vid1:
			return e

	# due to triangulation, we have to generate new edges
	e = ExportEdge( vid0, vid1 )
	edges.append( e )
	return e

def generate_faces( face, vid0, vid1, vid2 ):

	global export_faces
	global vertices
	global edges
	global faces
	global loops

	f = ExportFace()

	v0 = apply_mat_pos( vertices[vid0].co )
	v1 = apply_mat_pos( vertices[vid1].co )
	v2 = apply_mat_pos( vertices[vid2].co )

	f.normal = compute_normal( v0, v1, v2 )

	f.verts.append( ExportVertice( vid0, f, not smooth_normals ) )
	f.verts.append( ExportVertice( vid1, f, not smooth_normals ) )
	f.verts.append( ExportVertice( vid2, f, not smooth_normals ) )

	f.edges.append( retrieve_edges( face, vid0, vid1 ) )
	f.edges.append( retrieve_edges( face, vid1, vid2 ) )
	f.edges.append( retrieve_edges( face, vid2, vid0 ) )

	# pushing pos and uvs to final output directly
	add_pos( v0 )
	add_pos( v1 )
	add_pos( v2 )

	export_faces.append( f )


def face_based_export():
	
	global faces
	global vertices
	global log
	global all_indices
	global fcount
	global uvcount
	global shape_keys
	global export_keyshape
	global export_faces

	for face in faces:
		vs = face.vertices
		vnum = len(vs)
		if vnum == 4:
			generate_faces( face, vs[0],vs[1],vs[2] )
			generate_faces( face, vs[2],vs[3],vs[0] )
			add_uv( face.loop_start+0 )
			add_uv( face.loop_start+1 )
			add_uv( face.loop_start+2 )
			add_uv( face.loop_start+2 )
			add_uv( face.loop_start+3 )
			add_uv( face.loop_start+0 )
		if vnum == 3:
			generate_faces( face, vs[0],vs[1],vs[2] )
			add_uv( face.loop_start+0 )
			add_uv( face.loop_start+1 )
			add_uv( face.loop_start+2 )

	print( len(export_faces) )

	# connect vertices to faces
	for face in export_faces:
		for v in face.verts:
			v.connect_to_faces( export_faces )
			v.compute_normal()

	fcount = 0
	for f in export_faces:
		if len( all_indices ) != 0:
			all_indices += ","
		i = 0
		l = len( f.verts )
		while i < l:
			# add_normal(f.verts[i].normal)
			all_indices += str( fcount * 3 + i )
			if i < len( f.verts ) - 1:
				all_indices += ","
			i += 1
		fcount += 1
			
	log += "vertices added: " + str(vcount) + "\n"
	log += "faces added: " + str(fcount) + "\n"
	log += "uv added: " + str(uvcount) + "\n"

	for kb in shape_keys:

		export_keyshape = new_keyshape(kb.name)
		efid = 0

		for face in faces:

			vnum = len(face.vertices)
			vs = []

			for i in range(0,vnum):
				vs.append( apply_mat_pos( kb.data[face.vertices[i]].co ) )

			if len( export_keyshape["vertices"] ) > 0:
				export_keyshape["vertices"] += ","

			if vnum == 4:

				norm = compute_normal( vs[0],vs[1],vs[2] )
				export_faces[ efid ].normal = norm
				efid += 1

				norm = compute_normal( vs[2],vs[3],vs[0] )
				export_faces[ efid ].normal = norm
				efid += 1
				export_keyshape["vertices"] += to_string_co( vs[0] ) + ","
				export_keyshape["vertices"] += to_string_co( vs[1] ) + ","
				export_keyshape["vertices"] += to_string_co( vs[2] ) + ","
				export_keyshape["vertices"] += to_string_co( vs[2] ) + ","
				export_keyshape["vertices"] += to_string_co( vs[3] ) + ","
				export_keyshape["vertices"] += to_string_co( vs[0] )
				export_keyshape["vcount"] += 6
				export_keyshape["vncount"] += 6

			elif vnum == 3:

				norm = compute_normal( vs[0],vs[1],vs[2] )
				export_faces[ efid ].normal = norm
				efid += 1
				export_keyshape["vertices"] += to_string_co( vs[0] ) + ","
				export_keyshape["vertices"] += to_string_co( vs[1] ) + ","
				export_keyshape["vertices"] += to_string_co( vs[2] )
				export_keyshape["vcount"] += 3
				export_keyshape["vncount"] += 3
		
		for f in export_faces:

			if len( export_keyshape["vertex_normals"] ) != 0:
				export_keyshape["vertex_normals"] += ","

			i = 0
			l = len( f.verts )
			for v in f.verts:

				v.compute_normal()
				export_keyshape["vertex_normals"] += to_string_co( v.normal )
				if i < l -1:
					export_keyshape["vertex_normals"] += ","
				i += 1

		log += "keyshape " + kb.name + " added: " + str(export_keyshape["vcount"]) + " pos\n"
		all_keyshapes.append( export_keyshape )

def vertices_based_export():
	
	global vertices
	global faces
	global loops
	global log
	global all_indices
	global fcount
	global uvcount
	global shape_keys
	global export_keyshape
	
	print( "Warning: vertices_based_export has no uvs nor normals, but generates far smaller files" )

	for v in range(0,len(vertices)):
		add_vertex( v )
		add_uv( v )

	fcount = 0
	for face in faces:
		vnum = len(face.vertices)
		if len( all_indices ) != 0:
			all_indices += ","
		if vnum == 4:
			all_indices += str( face.vertices[0] ) + ","
			all_indices += str( face.vertices[1] ) + ","
			all_indices += str( face.vertices[2] ) + ","
			fcount += 1
			all_indices += str( face.vertices[2] ) + ","
			all_indices += str( face.vertices[3] ) + ","
			all_indices += str( face.vertices[0] )
			fcount += 1
		elif vnum == 3:
			all_indices += str( face.vertices[0] ) + ","
			all_indices += str( face.vertices[1] ) + ","
			all_indices += str( face.vertices[2] ) + ","
			fcount += 1
			
	log += "vertices added: " + str(vcount) + "\n"
	log += "faces added: " + str(fcount) + "\n"
	log += "uv added: " + str(uvcount) + "\n"

	for kb in shape_keys:
		export_keyshape = new_keyshape(kb.name)
		#print(kb.name, len(kb.data),len(vertices))
		kvcount = 0
		for i in range(0,len(kb.data)):
			if len( export_keyshape["vertices"] ) > 0:
				export_keyshape["vertices"] += ","
			export_keyshape["vertices"] += apply_mat( kb.data[i].co )
			export_keyshape["vcount"] += 1
		log += "keyshape " + kb.name + " added: " + str(export_keyshape["vcount"]) + " pos\n"
		all_keyshapes.append( export_keyshape )

'''
do collect
'''

if face_based_enabled:
	face_based_export()
else:
	vertices_based_export()

'''
packing everything in a json
'''

ctab = '\t'
cline = '\n'
if minimize_json:
	ctab = ''
	cline = ''

json_content = "{"+cline
json_content += ctab+"\"info\":{"+cline
json_content += ctab+ctab+"\"vertices\":" + str(vcount) + ","+cline
json_content += ctab+ctab+"\"normals\":" + str(ncount) + ","+cline
json_content += ctab+ctab+"\"colors\":" + str(ccount) + ","+cline
json_content += ctab+ctab+"\"faces\":" + str(fcount) + ","+cline
json_content += ctab+ctab+"\"uvs\":" + str(uvcount) + ","+cline
json_content += ctab+ctab+"\"keyshapes\":{"+cline
i = 0
for ksn in all_keyshapes:
	json_content += ctab+ctab+ctab+"\""+ksn["name"]+"\":{"+cline
	json_content += ctab+ctab+ctab+ctab+"\"vertices\":" + str(ksn["vcount"])+","+cline
	json_content += ctab+ctab+ctab+ctab+"\"vertex_normals\":" + str(ksn["vncount"])+cline
	json_content += ctab+ctab+ctab+"}"
	if i != len(all_keyshapes)-1:
		json_content += ","
	json_content += ""+cline
	i += 1
json_content += ctab+ctab+"}"+cline
json_content += ctab+"},"+cline
json_content += ctab+"\"vertices\":[" + all_pos + "],"+cline
json_content += ctab+"\"normals\":[" + all_norms + "],"+cline
json_content += ctab+"\"colors\":[" + all_colors + "],"+cline
json_content += ctab+"\"uvs\":[" + all_uvs + "],"+cline
json_content += ctab+"\"indices\":[" + all_indices + "],"+cline
json_content += ctab+"\"morphs\":{"+cline
i = 0
for ksn in all_keyshapes:
	json_content += ctab+ctab+"\""+ ksn["name"] +"\":{"+cline
	json_content += ctab+ctab+ctab+"\"vertices\":[" + ksn["vertices"] +"],"+cline
	json_content += ctab+ctab+ctab+"\"vertex_normals\":[" + ksn["vertex_normals"] +"]"+cline
	json_content += ctab+ctab+"}"
	if i != len(all_keyshapes)-1:
		json_content += ","
	json_content += ""+cline
	i += 1
		
json_content += ctab+"}"+cline
json_content += "}"+cline

f = open( export_path, "w+" )
f.write( json_content )
f.close()

print( log )