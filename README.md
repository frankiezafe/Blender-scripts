# Blender-scripts

various scripts to add functionalities to my favourite 3D software

## bpy

### my scripts

#### three.js

- **custom_threejs_keyshapes_export.py** : export a mesh's geometry (position, normal & uv) and its keyshapes in an easy and understandable json - prepared for buffergeometries - see class PantonPouf.js in [sisyphus](https://gitlab.com/frankiezafe/sisyphus) for loading implementation

configuration in blender, with sharp edges in cyan
![blender](http://polymorph.cool/wp-content/uploads/2018/05/morphnormales-threejs-blender-300x285.png)

result of the export with smooth_normals to False
![sharp](http://polymorph.cool/wp-content/uploads/2018/05/morphnormals-threejs-sharp-300x257.png)

result of the export with smooth_normals to True
![sharp](http://polymorph.cool/wp-content/uploads/2018/05/morphnormals-threejs-smooth-300x257.png)

- **empty_threejs_animation_export.py** : export all actions of the specified object into a custom json format

#### batch import & operations

- **batch-join.py** : join object by name based on the presence of a marker (to be adapted manually), string before the marker is considered as relevant, to be executed in object mode!

- **batch-obj-importer.py** : import all models form a folder, based on extension, works with *batch-join.py*

- **batch-select.py** : select all object starting with the needle (to be adapted manually)

### generation

- **material_assignation.py** : materials assignation to vertex groups, materials have to created first

- **material_creation.py** : materials creation

- **mesh-merge-and-split.py**: join all the object, create a new uv map for all and then split them back into separated meshes

- **roof-generator.py**: generation of triangular roof on simple parallelipipeds

- **roof-generator.py**: generation of triangular roof on simple parallelipipeds

- **uv_project_from_view.py**: set the 3D viewport in top view and generate UV from that view point

- **vectiler-processor.py**: [vectiler](http://karim.naaji.fr/projects/vectiler) import script, see comments in the script for more details

- **vertex-group-creation.py**: generation of vertex groups for armature

#### key shape

- **keyshapes_transfer.py** : transfer key shapes form one object to another having exactly the same topology.

#### armature

- **BatchBonesModification.py** : modification of bone parameters in batch

- **BonesVertexgroupsSync.py** : synchronise vertex groups to an armature, add and remove vertex groups and sort them by name

- **vertex-group-creation.py** : create vertex groups in batch

### other's

- **AFrameExporterScript.py** : AFrame exporter for blender, written by @Utopiah, source: https://gist.github.com/Utopiah/a2b9c6ecdb24ca8fd6f4f41a9c0eb32ed

- **export_svg.py** : generation of svg from viewport, found here: https://docs.blender.org/manual/en/dev/render/freestyle/export_svg.html
