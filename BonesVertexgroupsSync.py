import bpy
import collections

# name of the armature and the related mesh
ARMATURE_NAME = 'armature_skeleton'
MESH_NAMES = ['skeleton']
REMOVE_VERTEXGROUPS = True
SORT_VERTEXGROUPS = True

scn = bpy.context.scene
armature = scn.objects[ ARMATURE_NAME ].data
meshes = []
for name in MESH_NAMES:
	m = scn.objects[ name ]
	if m != None:
		meshes.append( m )

# creation of missing vertex groups
for b in armature.bones:
	for m in meshes:
		found = False
		for vg in m.vertex_groups:
			if vg.name == b.name:
				found = True
		if found == False:
			m.vertex_groups.new( b.name )
			print( b.name, "vertex group created" )

# deletion of vertex groups
if REMOVE_VERTEXGROUPS == True:
	for m in meshes:
		for vg in m.vertex_groups:
			found = False
			for b in armature.bones:
				if vg.name == b.name:
					found = True
			if found == False:
				print( vg.name, "vertex group removed" )
				m.vertex_groups.remove( vg )

if SORT_VERTEXGROUPS == True:
	for m in meshes:
		bpy.ops.object.select_all(action='DESELECT')
		bpy.ops.object.select_pattern(pattern=m.name)
		bpy.ops.object.vertex_group_sort()

	bpy.ops.object.select_all(action='DESELECT')
