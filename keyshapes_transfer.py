import bpy

source_mesh = "Cube.001"
source_key = "crash2"

target_mesh = "pouf_mesh"
target_key = "crash2"

key_source = bpy.data.meshes[source_mesh].shape_keys.key_blocks[source_key]
key_target = bpy.data.meshes[target_mesh].shape_keys.key_blocks[target_key]

for i in range(0, len(key_source.data)):
    #print( source.data[i].co )
    try:
        for j in range(0,3):
            key_target.data[i].co[j] = key_source.data[i].co[j]
    except:
        print( "Failed to copy keyshape..." )
        break