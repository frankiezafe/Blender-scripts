import bpy
import mathutils

print( "NEW RUN" )

def get_context(_type='VIEW_3D'):
    for window in bpy.context.window_manager.windows:
        screen = window.screen
        for area in screen.areas:
            if area.type == _type:
                for space in area.spaces:
                    if space.type == _type:
                        for region in area.regions:
                            if region.type == 'WINDOW':
                                context = {'window': window, 'screen': screen, 'area': area,
                                           'region': region, 'edit_object': bpy.context.edit_object,
                                           'scene': bpy.context.scene, 'blend_data': bpy.context.blend_data}
                                return context

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        break
for region in area.regions:
    if region.type == "WINDOW":
        break
space = area.spaces[0]
space.region_3d.view_perspective = 'ORTHO'
space.region_3d.view_matrix = ((1.0,0.0,0.0,0.0),(0.0,1.0,0.0,0.0),(0.0,0.0,1.0,0.0),(0.0,0.0,-0.047,1.0))
space.region_3d.view_location = bpy.context.scene.objects.active.location

c = get_context()

#bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.uv.project_from_view( camera_bounds=False, orthographic=True, correct_aspect=True, clip_to_bounds=True, scale_to_bounds=True)
#bpy.ops.object.mode_set(mode='OBJECT')

print( "DONE" )