import bpy

'''
select all object starting with the needle
execute in object mode!
'''

needle = "NV_information"

bpy.ops.object.select_all(action='DESELECT')

for obj in bpy.context.scene.objects:
    if obj.name.startswith( needle ):
        obj.select = True