import bpy

obj = bpy.context.scene.objects.active

bpy.ops.object.material_slot_add()
bpy.ops.object.material_slot_add()

obj.material_slots[0].material = bpy.data.materials[ "ATVB_mat_walls" ]
obj.material_slots[1].material = bpy.data.materials[ "ATVB_mat_roof" ]

bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.mesh.select_all(action='DESELECT')
bpy.context.object.active_material_index = 0
bpy.context.object.vertex_groups.active_index = 0
bpy.ops.object.vertex_group_select()
bpy.ops.object.material_slot_assign()
bpy.ops.mesh.select_all(action='DESELECT')
bpy.context.object.active_material_index = 1
bpy.context.object.vertex_groups.active_index = 1
bpy.ops.object.vertex_group_select()
bpy.ops.object.material_slot_assign()
bpy.ops.object.mode_set(mode='OBJECT')