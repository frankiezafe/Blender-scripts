import bpy
import math
import mathutils
import random

#print( "******** new run ********" )

obj = bpy.context.scene.objects.active

BORDER_MAX = 6

UP = mathutils.Vector((0,0,1))
FRONT = mathutils.Vector((0,1,0))
RIGHT = mathutils.Vector((1,0,0))
DIR_FRONT = mathutils.Vector((0,0,0))
DIR_RIGHT = mathutils.Vector((0,0,0))
TOP_CENTER = mathutils.Vector((0,0,0))

BORDER_VERTICES_NUM = 0
TOPZ = -1
BOTTOMZ = obj.dimensions[2] * 0.5
biggestfront = False
#print( obj.data.polygons )

bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.mesh.remove_doubles( threshold=0.01, use_unselected=True )
bpy.ops.object.mode_set(mode='OBJECT')

for v in obj.data.vertices:
	v.select = False

for e in obj.data.edges:
	e.select = False

for f in obj.data.polygons:
	d = UP.dot( f.normal )
	if d > 0.99:
		f.select = True
		TOPZ = f.center[2]
	else:
		d = FRONT.dot( f.normal )
		if biggestfront == False or biggestfront < d:
			DIR_FRONT = f.normal.copy()
			biggestfront = d 
		f.select = False

for v in obj.data.vertices:
	if v.co[2] == TOPZ:
		TOP_CENTER[0] += v.co[0]
		TOP_CENTER[1] += v.co[1]
		BORDER_VERTICES_NUM += 1

if BORDER_VERTICES_NUM > BORDER_MAX:
	print( "TOO MANY FACES!" )

TOP_CENTER.normalize()

bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.mesh.delete(type='FACE')
bpy.ops.object.mode_set(mode='OBJECT')

DIR_RIGHT = DIR_FRONT.cross( UP )

#print( DIR_FRONT )
#print( DIR_RIGHT )

sizef = 0
sizer = 0

smallest_size = 0
smallest_dir = 0

for e in obj.data.edges:
	d0 = obj.data.vertices[ e.vertices[0] ].co[2] - TOPZ
	d1 = obj.data.vertices[ e.vertices[1] ].co[2] - TOPZ
	if abs( d0 ) < 1e-5 and abs( d1 ) < 1e-5:
		obj.data.vertices[ e.vertices[0] ].select == True
		obj.data.vertices[ e.vertices[1] ].select == True
		e.select = True
	else:
		e.select = False
	if e.select == True:
		v0 = obj.data.vertices[ e.vertices[0] ].co
		v1 = obj.data.vertices[ e.vertices[1] ].co
		dir = mathutils.Vector(( v1[0]-v0[0], v1[1]-v0[1], v1[2]-v0[2] ))
		dirlen = dir.length
		dir.normalize()
		df = DIR_FRONT.dot( dir )
		dr = DIR_RIGHT.dot( dir )
		#print( dir, e, df, dr )
		if df > 0 and abs( df - 1 ) < 1e-5:
			#print( ">>>> front", dir )
			sizef = dirlen
		elif dr > 0 and abs( dr - 1 ) < 1e-5:
			#print( ">>>> right", dir )
			sizer = dirlen

#print( sizef, sizer )

front_switch = False
smallest_size = sizef
smallest_dir = DIR_FRONT
other_dir = DIR_RIGHT
scale = ( 0,1,1 )
scale_axis = ( True, False, False )
if smallest_size > sizer:
	front_switch = True
	smallest_size = sizer
	smallest_dir = DIR_RIGHT
	other_dir = DIR_FRONT
	#scale = ( 1,0,1 )
	#scale_axis = ( False, True, False )

bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.mesh.select_mode(type="VERT")

bpy.ops.mesh.mark_seam(clear=False)

# extrusion of top faces
roof_height_min = 0.3
roof_height_max = 0.6
extrudz = obj.dimensions[2] * ( roof_height_min + random.random() * ( roof_height_max - roof_height_min ) )
bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_translate={"value":(0,0,extrudz), "constraint_axis":(False, False, False), "constraint_orientation":'GLOBAL', "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "gpencil_strokes":False, "texture_space":False, "remove_on_cancel":False, "release_confirm":False})

# adding a face to generate correct normal
bpy.ops.mesh.edge_face_add()
bpy.ops.mesh.normals_make_consistent(inside=False)
bpy.ops.object.mode_set(mode='OBJECT')

NEW_FRONT = mathutils.Vector((0,0,0))
for f in obj.data.polygons:
	if f.select:
	
		#print( ">>>>>>>>>> f.normal", f.normal )
		
		if BORDER_VERTICES_NUM <= 4:
			seg0 = mathutils.Vector((
				obj.data.vertices[f.vertices[1]].co[0] - obj.data.vertices[f.vertices[0]].co[0],
				obj.data.vertices[f.vertices[1]].co[1] - obj.data.vertices[f.vertices[0]].co[1],
				obj.data.vertices[f.vertices[1]].co[2] - obj.data.vertices[f.vertices[0]].co[2] 
				)).normalized()
			seg1 = mathutils.Vector((
				obj.data.vertices[2].co[0] - obj.data.vertices[1].co[0],
				obj.data.vertices[2].co[1] - obj.data.vertices[1].co[1],
				obj.data.vertices[2].co[2] - obj.data.vertices[1].co[2]
				)).normalized()
			NEW_FRONT = seg0
		else:
			for vi in f.vertices:
				NEW_FRONT += obj.data.vertices[ vi ].normal
			NEW_FRONT.normalize()
			
		#if abs( NEW_FRONT[0] ) < 1e-5 and abs( NEW_FRONT[1] ) < 1e-5:
		#	NEW_FRONT = FRONT.copy()
		#if BORDER_VERTICES_NUM == 4:
		#	NEW_FRONT = UP.cross( NEW_FRONT )
		
		break

#print( ">>>>>>>>>> NEW_FRONT", NEW_FRONT )

bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.mesh.quads_convert_to_tris(quad_method='BEAUTY', ngon_method='BEAUTY')
bpy.ops.mesh.select_all(action='DESELECT')
bpy.ops.object.mode_set(mode='OBJECT')

# counting top vertices
topvs = []
topvs_opposite = []

# selection of top vertices
for v in obj.data.vertices:
	if v.co[2] > TOPZ:
		nv = mathutils.Vector( ( v.co[0] - TOP_CENTER[0], v.co[1] - TOP_CENTER[1], 0 ) )
		nv.normalize()
		if NEW_FRONT.dot( nv ) > 0:
			topvs.append( (v.co[0], v.co[1], v.co[2]) )
		else:
			topvs_opposite.append( (v.co[0], v.co[1], v.co[2]) )

if len( topvs ) < len( topvs_opposite ):
	#print( ">>>>>>>>>> LIST SWICTH!" )
	tmp = topvs_opposite
	topvs_opposite = topvs
	topvs = tmp

for pv in topvs:
	v0 = False
	v0index = False
	i = 0
	for v in obj.data.vertices:
		if (
			pv[0] == v.co[0] and
			pv[1] == v.co[1] and
			pv[2] == v.co[2]
			):
				v0 = v
				v0index = i
				break
		i += 1
	
	v1 = False
	smallest_d = 0
	opp_i = -1
	for e in obj.data.edges:
		o = -1
		if e.vertices[0] == v0index:
			o = e.vertices[1]
		elif e.vertices[1] == v0index:
			o = e.vertices[0]
		tmpo = obj.data.vertices[o]
		keepon = False
		oi = 0
		
		if len( topvs_opposite ):
			for t in topvs_opposite:
				if (
					t[0] == tmpo.co[0] and
					t[1] == tmpo.co[1] and
					t[2] == tmpo.co[2]
					):
					keepon = True
					break
				oi += 1
				
		elif tmpo.co[2] > TOPZ:
			keepon = True
			
		if keepon == False:
			continue
			
		nv = mathutils.Vector( ( tmpo.co[0] - v0.co[0], tmpo.co[1] - v0.co[1], 0 ) )
		nv.normalize()
		d = abs( NEW_FRONT.dot( nv ) )
		if v1 == False or smallest_d < d:
			v1 = tmpo
			smallest_d = d
			if len( topvs_opposite ):
				opp_i = oi
	
	if  opp_i != -1:
		topvs_opposite = topvs_opposite[:opp_i] + topvs_opposite[opp_i+1:]
	
	#print( "merging" )
	#print( v0 )
	#print( v1 )
	
	v0.select = True
	v1.select = True
	
	bpy.ops.object.mode_set(mode='EDIT')
	bpy.ops.mesh.merge(type='CENTER')
	bpy.ops.mesh.select_all(action='DESELECT')
	bpy.ops.object.mode_set(mode='OBJECT')

bpy.ops.object.mode_set(mode='EDIT')
#bpy.ops.mesh.merge(type='CENTER')
#bpy.context.scene.tool_settings.use_mesh_automerge = True
#bpy.ops.transform.resize(value=scale, constraint_axis=scale_axis, constraint_orientation='NORMAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)
bpy.ops.mesh.select_all(action='SELECT')
#cleanup
bpy.ops.mesh.remove_doubles()
#fix normals
bpy.ops.mesh.normals_make_consistent(inside=False)
bpy.ops.mesh.select_all(action='DESELECT')
bpy.ops.object.mode_set(mode='OBJECT')

# generation of seams
for e in obj.data.edges:
	v0 = obj.data.vertices[ e.vertices[0] ]
	v1 = obj.data.vertices[ e.vertices[1] ]
	if v0.co[2] > TOPZ or v1.co[2] > TOPZ:
		e.select = True
	else:
		e.select = False
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.mesh.mark_seam(clear=False)
bpy.ops.object.mode_set(mode='OBJECT')

# generation of vertex groups
bpy.ops.object.vertex_group_add()
obj.vertex_groups[ obj.vertex_groups.active_index ].name = 'roof'
bpy.ops.object.vertex_group_set_active(group='roof')
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.object.vertex_group_select()
bpy.ops.mesh.select_all(action='DESELECT')
bpy.ops.object.mode_set(mode='OBJECT')
for f in obj.data.polygons:
	if f.center[2] >= TOPZ:
		for vi in f.vertices:
			obj.data.vertices[vi].select = True
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.object.vertex_group_assign()
bpy.ops.object.mode_set(mode='OBJECT')

bpy.ops.object.vertex_group_add()
obj.vertex_groups[ obj.vertex_groups.active_index ].name = 'walls'
bpy.ops.object.vertex_group_set_active(group='walls')
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.object.vertex_group_select()
bpy.ops.mesh.select_all(action='DESELECT')
bpy.ops.object.mode_set(mode='OBJECT')
for f in obj.data.polygons:
	if f.center[2] <= TOPZ:
		for vi in f.vertices:
			obj.data.vertices[vi].select = True
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.object.vertex_group_assign()
bpy.ops.object.mode_set(mode='OBJECT')

# finishing by moving the origin to the floor of the model
bpy.context.scene.cursor_location = (obj.location[0], obj.location[1], obj.location[2] - BOTTOMZ )
bpy.ops.object.origin_set(type='ORIGIN_CURSOR')
