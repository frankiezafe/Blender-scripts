import bpy
import mathutils

SCALE = [0.4,0.03,0.03]
UP = mathutils.Vector((0,0,1))

# get active object
obj = bpy.context.view_layer.objects.active

# get all vertices of active object
verts = []
normals = []
for v in obj.data.vertices:
    verts.append( v.co )
    normals.append( mathutils.Vector((v.normal[0],v.normal[1],v.normal[2])) )

print( verts )

bpy.ops.object.empty_add()
root = bpy.context.view_layer.objects.active

for i in range(0, len(verts) ):
    
    # getting data
    v = verts[i]
    n = normals[i]
    
    # converting normal to rotation
    # see https://docs.blender.org/api/current/mathutils.html?highlight=vector%20mathutils#mathutils.Vector
    q = n.to_track_quat( 'X', 'Z' )
    
    # creating cubes
    bpy.ops.mesh.primitive_cube_add()
    cube = bpy.context.view_layer.objects.active
    cube.location = v
    cube.scale = SCALE
    # by default, rotation are expressed in euler angle XYZ order
    cube.rotation_euler = q.to_euler( 'XYZ' )
    cube.parent = root