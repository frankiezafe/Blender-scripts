'''

 _________ ____  .-. _________/ ____ .-. ____
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'

 libre video game bazaar

 ____________________________________  ?   ____________________________________
                                     (._.)

 Copyright (c) 2018 françois zajéga
 All rights reserved.

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

 ___________________________________( ^3^)_____________________________________

 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)

'''

'''

Javasript to load result of this export:

function json_load_success( e ) {

	try {

		var data = JSON.parse( e );

		var mixer = new THREE.AnimationMixer(this.butt);
		var animations = {};

		for ( var tname in data ) {

			var tracks = [];
			var max_duration = 0;

			for ( subt in data[tname] ) {

				if ( subt == "location" ) {

					// turning into VectorKeyframeTrack
					var times = [];
					var values = [];
					for ( var frame in data[tname][subt] ) {
						var time = parseInt(frame);
						if ( max_duration < time ) {
							max_duration = time;
						}
						times.push( time );
						var vs = data[tname][subt][frame];
						values.push( vs[0] );
						values.push( vs[1] );
						values.push( vs[2] );
					}
					var t = new THREE.VectorKeyframeTrack( tname+".position", times, values );
					tracks.push( t );

				} else if ( subt == "rotation_euler" ) {

					// turning into QuaternionKeyframeTrack
					var times = [];
					var values = [];
					for ( var frame in data[tname][subt] ) {
						var time = parseInt(frame);
						if ( max_duration < time ) {
							max_duration = time;
						}
						times.push( time );
						var vs = data[tname][subt][frame];
						var q = new THREE.Quaternion();
						q.setFromEuler( new THREE.Euler( vs[0], vs[1], vs[2], 'XYZ' ) );
						q.toArray( vs );
						values.push( vs[0] );
						values.push( vs[1] );
						values.push( vs[2] );
						values.push( vs[3] );keylist = mydict.keys()
keylist.sort()
					}
					var t = new THREE.QuaternionKeyframeTrack( tname+".quaternion", times, values );
					tracks.push( t );

				}

			}

			var clip = new THREE.AnimationClip( tname , max_duration, tracks );
			var action = this.butt_mixer.clipAction( clip );
			animations[tname] = action;

		}

		if ( this.pouf !== undefined ) {
			this.all_loaded();
		}

	} catch( exception ) {

		console.log( "parsing of " + json_path + " failed, \n" + exception );

	}

};

'''

import bpy
import math
import mathutils
from collections import OrderedDict

# obect name
export_obj = "butt"
# turn this on to remove tabs and line breaks in json
minimize_json = False
# export path
export_path = "/home/frankiezafe/projects/sisyphus/public/models/butt_anims.json"

obj = bpy.data.objects[export_obj]

actions = []
tracks = {}

'''
functions
'''

def new_track( name ):
    if name in tracks:
        return tracks[ name ]
    return {
        'name': name,
        'location': {},
        'rotation_euler': {}
    }

def apply_mat_pos( co ):

	global correction_pos

	mat = mathutils.Matrix.Translation((co[0],co[1],co[2]))
	mat = correction_pos * mat
	return mat.to_translation()

def apply_mat_euler( eul ):

	global correction_pos

	mat = eul.to_matrix().to_4x4()
	mat = correction_pos * mat
	return mat.to_euler('XYZ')

def to_string_co( v ):

	if len( v ) == 2:
		return str(v[0]) + "," + str(v[1])

	elif len( v ) == 3:
		return str(v[0]) + "," + str(v[1]) + "," + str(v[2])

'''
collecting actions
'''

correction_pos = mathutils.Matrix.Identity(4)
axis_mat = mathutils.Matrix.Identity(4)
axis_mat[0] = [1,0,0,0]
axis_mat[1] = [0,0,1,0]
axis_mat[2] = [0,-1,0,0]
correction_pos = axis_mat * correction_pos

for nla in obj.animation_data.nla_tracks:
    for strip in nla.strips:
        actions.append( strip.action )

'''
parsing actions
'''

for action in actions:
    for g in action.groups:
        t = new_track( action.name )
        for c in g.channels:
            if c.data_path in t:
                for k in c.keyframe_points:
                    frame = int(k.co[0])
                    if c.data_path == 'rotation_euler':
                        if not frame in t[c.data_path]:
                            t[c.data_path][frame] = mathutils.Euler((0.0,0.0, 0.0), 'XYZ')
                    elif c.data_path == 'location':
                        if not frame in t[c.data_path]:
                            t[c.data_path][frame] = mathutils.Vector((0.0,0.0, 0.0))
                    else:
                        continue
                    t[c.data_path][frame][c.array_index] = k.co[1]
        tracks[action.name] = t

ctab = '\t'
cline = '\n'
if minimize_json:
	ctab = ''
	cline = ''

json_content = "{"+cline

tracks_content = ""
for t in tracks:

    track = tracks[t]
    track_data = ctab+"\"" + t + "\":{"+cline
    track_list = ""

    for k in track:

        #print("**************")

        if k == "name":
            continue

        subt = ""
		track[k] = ( OrderedDict(sorted(track[k].items(), key=lambda t: t[0])) )
        for frame in track[k]:

            if k == "rotation_euler":
                #print( track[k][frame]  )
                e = apply_mat_euler( track[k][frame]  )
                #print( e )
                if len( subt ) > 0:
                    subt += ","+cline
                subt += ctab+ctab+ctab+"\""+ str(frame) + "\":[" + to_string_co(e) + "]"

            if k == "location":
                #print( track[k][frame]  )
                v = apply_mat_pos( track[k][frame]  )
                #print( v )
                if len( subt ) > 0:
                    subt += ","+cline
                subt += ctab+ctab+ctab+"\""+ str(frame) + "\":[" + to_string_co(v) + "]"

        if len( subt ) > 0:
            if len( track_list ) > 0:
                    track_list += ","+cline
            track_list += ctab+ctab+"\""+k+"\":{" + cline + subt + cline + ctab + ctab + "}"

    track_data += track_list + cline + ctab + "}"
    if len( tracks_content ) > 0:
        tracks_content += ","+cline
    tracks_content += track_data

json_content += tracks_content +cline
json_content += "}"

f = open( export_path, "w+" )
f.write( json_content )
f.close()

print( export_path, "generated" )
#print( json_content )
