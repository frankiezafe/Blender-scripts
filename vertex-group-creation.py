import bpy

# select the object to attack first

print( bpy.context.object )

for i in range( 1,16 ):
    
    vgn = "vertebra_"
    if i < 10:
        vgn += "0"
    vgn += str( i ) + "_body"
    bpy.ops.object.vertex_group_add()
    bpy.context.object.vertex_groups[ i - 1 ].name = vgn
